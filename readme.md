# LPR5 flasher

Tools to flash the firmware to the LPR5 units as well as to lauch the QC system.

## Getting Started

Included tools:

-  `launchLPR5Flasher.sh`: flasher launcher.
-  `launchDhcp.sh`: DHCP server launcher (just for 2 ethernes scenarios).
-  `launchLPR5QC.sh`: QC launcher.
-  `update.sh`: updates the system to the last version.

### Prerequisites

Docker must be installed. 

```
sudo apt-get install docker
```

## Tools

### LPR5Flasher

Run the LPR5 flasher:
```
./launchLPR5Flasher.sh --production
```

LPR5 flasher has a website that prints all the events of the flasher system
```
http://127.0.0.1
```

For further information about the app:
```
./launchLPR5Flasher.sh --help
```

### DHCP server

The dhcp launcher launches a dhcp server. 
This app must only be executed when flasher system is running over 2 ethernets (public: internet; private: units).

The dhcp server must run over an ethernet where mask=255.255.255.0 and ip=\*.\*.\*.1-8 (p.ex: 172.21.0.1).
By default, the moxa QC device uses the IP \*.\*.\*.9, so avoid using this one.

```
./launchDhcp.sh <eth>
```

For further information about the app:
```
./launchDhcp.sh --help
```

### Production QC

Run the LPR5 QC:
```
./launchLPR5QC.sh --production
```

Once started, select the same ethernet device than the DHCP server (very first dialog).

From the `configuration` menu, next variables can be customized:

- Sample license plate
- Moxa inputs/outputs and IP
- Test units: `NA` or `NoNA`
- QC mode: 
	- `process`: default mode
	- `skip`: connected LPR5 units won't perform the QC, but they will set as they did.



For further information about the app:
```
./launchLPR5QC.sh --help
```

### LPR5 production API

Use google-chrome/firefox to open the production server URL: https://prodapps.quercus-technologies.com 
This website provides information about all the lpr5 productions and QC's.

For development porposes: production server (develop) URL: https://quercus-app-uh254s4qpq-ew.a.run.app

### system update

Use this tool to update the system to the last version.

```
./update.sh
```


## Common issues

### launchLPRFlasher / Docker logout

If docker login error: “Error response from daemon: pull access denied for quercustechnologies/lpr5-flasher, repository does not exist or may require 'docker login': denied: requested access to the resource is denied”

- Create a dockerhub account.
- Contact with the IT Dpt to be included to the quercustechnologies organization and to have acces permissions to the lpr5 flasher repository.
- Login with your dockerhub account
```
docker login
```

### launchLPRFlasher / Tftp can't start

Sometimes tftpd-hpa can't execute, so launchLPR5Flasher is automatically closed once executed (no dialog pops-up).

When this happens, check if tftpd-hpa service is already running on your system:
```
sudo service tftpd-hpa status
sudo service tftpd-hpa stop
```

If it was not running, just install it  and remove it (it sounds stupid... but it works.)
```
sudo apt-get install tftpd-hpa
sudo apt-get remove tftpd-hpa
```

## Documentation

* Analysis: [google-drive](https://docs.google.com/document/d/15MIbxC0KA_Z1V_toZNnt7YHmOzjoyw6hg6l5oQ5Up-o)
* Developers tips: [dokuwiki](http://r2d2.quercus.biz/dokuwiki/doku.php?id=lpr5:newdevelopfirmware)

## Authors

* **Quercus Technologies - R&D**
