#!/bin/bash
set -euo pipefail

#---------------------------------------------------------------------
# (c) Quercus Technologies (jun'2020)
#---------------------------------------------------------------------
#Descr: Runs the LPR5 flasher or QC docker images
#---------------------------------------------------------------------
IsModuleLoaded_()
{
        lsmod | grep "$1" > /dev/null    2>&1
        return $?
}


Exit_()
{
        echo -e "$1"
        exit 1
}

KillDockerProcess_()
{
	containerId=$(echo $(docker container ls | grep $imageName | cut -d ' ' -f 1))
	if [ ${#containerId} -ge 5 ]; then
		echo "Kill previous execution" $imageName $containerId
		docker container kill $containerId
	fi
}

#---------------------------------------
# V A R I A B L E S    &    C H E C K S
#---------------------------------------
qc=0
[ ! -z "$(echo $(basename $0) | grep -i qc)" ] && qc=1
versionsFile="./Versions.cfg"
imageName="quercustechnologies/lpr5-flasher"
[ $qc -eq 1  ] && imageName="quercustechnologies/lpr5-productionqc"
defaultImageVersion="latest"
mode="--production"
imageVersion=""
pull=true

[ $qc -eq 0  ] && modules=( "nfs" "nfsd" )

while [ $# -gt 0 ]; do
	if [ "$1" == "--develop" ] || [ "$1" == "--production" ]; then
		mode=$1
		shift
	elif [ "$1" == "--local" ]; then
		pull=false
		shift
	elif [ -z "$imageVersion" ] && [ "$1" != "--help" ]; then
		imageVersion=$1
		shift
	else
		[ "$1" != "--help" ] && echo "Invalid parameters"
		echo -e "$(basename $0): [OPTIONS] [image version]"
		echo -e "\t--develop: develop mode"
		echo -e "\t--production: production mode (default)"
		echo -e "\t--local: does not try to pull the image (develop purposes)"
		echo -e "\t- default image version: $defaultImageVersion" 
		[ "$1" != "--help" ] && exit 1
		exit 0
	fi
done

[ -z "$imageVersion" ] && imageVersion="$defaultImageVersion"

image="$imageName:$imageVersion"

#---------------------------------------
# M A I N
#---------------------------------------
[ -z $(which docker) ] &&
        Exit_ "Docker is not installed!\nTry running: apt install docker"       

for module in ${modules[@]}; do
        IsModuleLoaded_ "$module" ||
                Exit_ "Module $module not loaded.\nTry running: modprobe $module"
done


if $pull; then
    docker pull "$image"
fi

KillDockerProcess_

if [ $qc -eq 0 ]; then
	docker run -it --rm --net=host --privileged "$image" $mode
else
	xhost local:docker && docker run -it --rm --net=host --privileged -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY "$image" $mode
fi

KillDockerProcess_
