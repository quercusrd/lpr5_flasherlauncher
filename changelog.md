# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3] - 2022-02-21
### Added
- Readme includes information about the new Production Server (GCP) environments.

### Changed
- index.html removed. Using new Production Server (GCP) website.

## [2] - 2021-09-21
### Changed
- index.html QC's table row limit can be selected between 5 or 20.

## [1] - 2021-09-01
### Added
- First Flasher Launcher version.
